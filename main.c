#include <stdio.h>

int calc_runtime(float darlehen, float zinssatz, float annuitaet);

float zinssumme = 0.0;

int main() {

    //Input
    int laufzeit;
    float b, c, darlehen, zinssatz, tilgung, annuitaet;
    printf("Hoehe der Darlehenssumme in Eurocent:\n");
    scanf("%f", &darlehen);
    printf("Jaehrlicher Zinssatz in Prozent:\n");
    scanf("%f", &b);
    printf("Hoehe der jaehrlichen anfaenglichen Tilgung in Prozent:\n");
    scanf("%f", &c);

    //Umrechnen
    zinssatz = b * 0.01;
    tilgung = c * 0.01;

    annuitaet = (darlehen * zinssatz / 12) + (darlehen * tilgung / 12);
    laufzeit = calc_runtime(darlehen, zinssatz, annuitaet);

    printf("Monatliche Annuitaet in Euro = %.2f \nLaufzeit in Monaten = %d \nTotale Zinssumme in Euro = %.2f",
           annuitaet, laufzeit, zinssumme);
    return 0;
}

//Laufzeit berechnen
int calc_runtime(float darlehen, float zinssatz, float annuitaet) {
    int laufzeit = 0;
    while (darlehen > 0) {
        zinssumme += darlehen * zinssatz / 12;
        darlehen = darlehen - (annuitaet - darlehen * zinssatz / 12);
        laufzeit++;
    }
    return laufzeit;
}